package controller;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

import Modelo.CuentaCorriente;
import Modelo.TarjetaCredito;

public class Metodos {
	// Metodo para calcular la edad
	// Instanciar clase CuentaCorriente
	public static int calculoEdad(String fechaNacimiento) {

		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
		LocalDate ahora = LocalDate.now();
		Period periodo = Period.between(fechaNac, ahora);
		return periodo.getYears();
		// System.out.printf("Tienes: %s a�os",periodo.getYears());

	}
	// End Metodo

	public static void retiroDinero(ArrayList<CuentaCorriente> ctaCorriente, double newRetiro) {

		ctaCorriente.get(0).setSaldo(ctaCorriente.get(0).getSaldo() - newRetiro);
		// 1500000-500000=1000000;
		// newRetiro=getSaldo-retiro
		//
		// System.out.println("Listo");

	}

	public static void pagarDeuda(ArrayList<CuentaCorriente> ctaCorriente, double pagoDeuda) {
		pagoDeuda = ctaCorriente.get(0).getDeuda() - pagoDeuda;

		ctaCorriente.get(0).setDeuda(pagoDeuda);
		System.out.println("Su deuda fue pagada");
	}
	
	public static void pagarDeudaTarjeta(ArrayList<TarjetaCredito> tarjetita) {
		Scanner src = new Scanner(System.in);
		int op;
		System.out.println("1. Deuda completa");
		System.out.println("2. Otro monto");
		
		op = src.nextInt();
		while (op < 1 || op > 2) {
			System.out.println("Debe ser entre 1 y 4 0/Ingrese nuevamente");
			op = src.nextInt();
		}
		switch (op) {
		case 1:
			System.out.println("Deuda completa pagada");
			tarjetita.get(0).setDeuda(tarjetita.get(0).getDeuda()-tarjetita.get(0).getDeuda());
			break;
		case 2:
			System.out.println("Ingresa el monto que deseas pagar (m�ximo" +tarjetita.get(0).getDeuda()+")");
			double pagoDeuda = src.nextDouble();
			while (pagoDeuda < tarjetita.get(0).getDeuda()) {
				System.out.println("No debe pagar mas que $ " + tarjetita.get(0).getDeuda());
				System.out.println("Ingrese nuevamente el monto");
				pagoDeuda = src.nextInt();
			}
			tarjetita.get(0).setDeuda(tarjetita.get(0).getDeuda() - pagoDeuda);
			System.out.println("Se ha reducido tu deuda en " + pagoDeuda);
			break;

		default:
			break;
		}


	}

}
