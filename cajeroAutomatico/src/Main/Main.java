package Main;

import controller.*;
import java.util.ArrayList;
import java.util.Scanner;

import Modelo.*;
import Utilidades.*;
import Modelo.*;

public class Main {

	public static Scanner src = new Scanner(System.in);

	static ArrayList<Cliente> cliente = new ArrayList<>();
	static ArrayList<CuentaCorriente> ctaCorriente = new ArrayList<>();
	static ArrayList<TarjetaCredito> tarjetita = new ArrayList<>();

	public static void main(String[] args) {
		cliente.add(new Cliente("01/03/1996", "Ruben", "Benavides", "19291774", "rabenavidess", "1234"));
		ctaCorriente.add(new CuentaCorriente(cliente.get(0), true, "00-19291774", 1500000, 1000000, 1234, 13000));
		tarjetita.add(new TarjetaCredito(cliente.get(0), true, "01-19291774", 1000000, 1200000, 1234, 1015000));
		Ejecutivo ejecutivo = new Ejecutivo("Alberto", "Valenzuela", "+569 5656 5656", "Uno", 1, 1);

		for (TarjetaCredito tarjeta : tarjetita) {
			System.out.println(tarjeta.toString());

			System.out.println("Ingrese su RUT");
			String rut = src.next();

			System.out.println("Ingrese su clave");
			String clave = src.next();
			int op;
			if (rut.equals(tarjeta.getCliente().getRut()) && clave.equals(tarjeta.getCliente().getPassword())) {

				System.out.println("Bienvenido " + tarjeta.getCliente().getNombre() + " "
						+ Metodos.calculoEdad(tarjeta.getCliente().getFechaNacimiento()) + " a�os");
				do {
					System.out.println("Que deseas hacer?");
					System.out.println("1- Menu Cuenta Corriente");
					System.out.println("2- Menu Tarjeta de Credito");
					System.out.println("3- Ver datos de mi ejecutivo");
					System.out.println("4- Salir");
					System.out.println("Ingrese opci�n 1 al 4");
					op = src.nextInt();
					while (op < 1 || op > 4) {
						System.out.println("Debe ser entre 1 y 4 0/Ingrese nuevamente");
						op = src.nextInt();
					}

					switch (op) {
					case 1:
						do {

							System.out.println("Cuenta Corriente " + ctaCorriente.get(0).getIdentificarProducto());
							System.out.println("Su saldo actual es: " + ctaCorriente.get(0).getSaldo());
							if (0 == ctaCorriente.get(0).getDeuda()) {
								System.out.println("No tienes deuda");
							} else {
								System.out.println("La deuda de su cuenta es: " + ctaCorriente.get(0).getDeuda());

							}
							System.out.println("Qu� desea hacer ?");
							System.out.println("1. Volver al menu");
							System.out.println("2. Retirar dinero");
							System.out.println("3. Pagar mi deuda");
							int op1 = src.nextInt();
							switch (op1) {
							case 1:
								break;
							case 2:
								if (ctaCorriente.get(0).getSaldo() == 0) {
									System.out.println("NO TIENES SALDO PARA RETIRAR");
								} else {
									System.out.println("Cu�nto desea retirar ? (0 para cancelar)");
									int retiro = src.nextInt();
									while (retiro > ctaCorriente.get(0).getSaldo()) {
										System.out
												.println("No debe retirar mas que $ " + ctaCorriente.get(0).getSaldo());
										System.out.println("Ingrese nuevamente el monto");
										retiro = src.nextInt();
									}
									Metodos.retiroDinero(ctaCorriente, retiro);
								}

								break;
							case 3:
								if (ctaCorriente.get(0).getDeuda() == 0) {
									System.out.println("NO TIENE DEUDA POR PAGAR");
								} else {
									System.out.println("cuanto desea pagar?");
									double pago;
									pago = src.nextDouble();
									while (pago > ctaCorriente.get(0).getDeuda()) {
										System.out.println("No debe pagar mas que $ " + ctaCorriente.get(0).getDeuda());
										System.out.println("Ingrese nuevamente el monto");
										pago = src.nextInt();
									}
									Metodos.pagarDeuda(ctaCorriente, pago);
								}

								break;

							}
						} while (op != 1);

						break;
					// tarjeta de credito
					case 2:
						

							if (tarjetita.get(0).getDeuda() == 0) {
								System.out.println("No tienes deuda");
								System.out.println("Volviendo al menu principal...");
							} else {

								System.out.println("Tarjeta de credito " + tarjetita.get(0).getIdentificarProducto());
								System.out.println("Su saldo actual es: " + tarjetita.get(0).getSaldo() + " / "
										+ tarjetita.get(0).getSaldoMaximo());
								if (0 == tarjetita.get(0).getDeuda()) {
									System.out.println("No tienes deuda");
								} else {
									System.out.println("La deuda de su cuenta es: " + tarjetita.get(0).getDeuda());
								}
								System.out.println("Qu� desea hacer ?");
								System.out.println("1. Volver al menu");
								System.out.println("2. Pagar mi deuda");
								int op1 = src.nextInt();
								switch (op1) {
								case 1:
									break;
								case 2:
									do {
									if (tarjetita.get(0).getDeuda() == 0) {
										System.out.println("NO TIENE DEUDA POR PAGAR");
									} else {

										Metodos.pagarDeudaTarjeta(tarjetita);
									}} while (op != 1);
									break;
								}

							}
						
						break;
					case 3:
						System.out.println("Datos de tu ejecutivo: ");
						System.out.println("Nombre: " + ejecutivo.getNombre() + " " + ejecutivo.getApellido()
								+ ", Telefono: " + ejecutivo.getTelefono() + ", Direccion: " + ejecutivo.getCalle()
								+ " - Depto " + ejecutivo.getDepto() + " - piso " + ejecutivo.getPiso());
						break;
					default:
						break;
					}
				} while (op != 4);

			} else {

				System.out.println("Usuario y/o contrase�a son incorrectas !");
			}
		}

	}

}
